# Patched Wine Installer

This is a free and open source installer of a build of Wine with additional patches. The build is from the Grapejuice Discord server.

**Disclaimer:** This project is not associated with Wine or Grapejuice.
