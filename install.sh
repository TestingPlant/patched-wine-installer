#!/bin/bash

name=debuntu-wine-tkg-staging-fsync-git-7.14.r0.g1d21e2b6.tar.zst
destination="${XDG_DATA_HOME:-$HOME/.local/share}/grapejuice/user/patched-wine-download"
config_dir="${XDG_CONFIG_HOME:-$HOME/.config}/brinkervii/grapejuice"
config="$config_dir/user_settings.json"
temporary_directory="$(mktemp -d)"

get_config() {
	if [ -f "$config" ]; then
		cat -- "$config"
	else
		echo "{}"
	fi
}

cd -- "$temporary_directory"

wget -- "https://cdn.discordapp.com/attachments/858117357897121822/1004171996202020864/$name"
mkdir -p -- "$destination"
tar --extract --file "$name" --directory "$destination" --strip-components=1

mkdir -p -- "$config_dir"
jq --arg destination "$destination" '. += {"default_wine_home": $destination}' <<< "$(get_config)" > "$config"

rm -r -- "$temporary_directory"

echo "Successfully installed"
